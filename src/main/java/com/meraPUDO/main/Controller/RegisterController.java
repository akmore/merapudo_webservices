package com.meraPUDO.main.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.meraPUDO.main.Models.Register;
import com.meraPUDO.main.Models.ResponseModel;

@RestController
public class RegisterController {
	
	@Autowired
	private com.meraPUDO.main.DAO.registerDAO registerDAO;

	@Autowired 
	private ResponseModel responseModel;
	
	//Register Controller to register new user
	@PostMapping("register_control/register")
	public ResponseModel addUSer(@RequestBody Register register) {
		int response = registerDAO.registerUser(register);
        System.out.println(response);
		if (response > 0)
			responseModel.setSuccessMessage(response);
		else
			responseModel.setFailureMessage(response);

		return responseModel;
	}
}

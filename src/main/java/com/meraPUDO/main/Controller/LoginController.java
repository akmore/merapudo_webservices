package com.meraPUDO.main.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.meraPUDO.main.Models.Login;
import com.meraPUDO.main.DAO.loginDAO;
import com.meraPUDO.main.Models.ResponseModel;
@RestController
public class LoginController {
	@Autowired
	private loginDAO loginDAO;

	@Autowired
	private ResponseModel responseModel;
	
	//Login Controller to check login
	@PostMapping("login_control/login")
	public ResponseModel getStatistics(@RequestBody Login login) {
		Boolean response = loginDAO.checkLogin(login);
        System.out.println(response);
		if (response == true)
			responseModel.setSuccessMessage(response);
		else
			responseModel.setFailureMessage(response);

		return responseModel;
	}
}

package com.meraPUDO.main.Models;


import org.springframework.stereotype.Component;

import com.meraPUDO.main.Globals.KeyConstants;

@Component
public class ResponseModel {
	private Object data;
	private String status;
	
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setSuccessMessage(Object data) {
		this.status = KeyConstants.SUCCESS;
		this.data = data;
	}
	public void setFailureMessage(Object data) {
		this.status = KeyConstants.FAILURE;
		this.data = data;		
	}
}



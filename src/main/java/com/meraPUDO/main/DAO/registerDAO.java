package com.meraPUDO.main.DAO;
import java.io.StringWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.meraPUDO.main.Models.Register;
@Repository
public class registerDAO {

	@Autowired
	JdbcTemplate jdbcTemplate;
	StringWriter sw = new StringWriter();
	
	public int registerUser(Register register) {	
		System.out.println(register);
		String query = "INSERT INTO user_master(user_id,user_name,user_pass,user_email,user_phone,user_address) VALUES(?,?,?,?,?,?)";	
		return jdbcTemplate.update(query, 2, register.getUser_name(), register.getUser_pass(), register.getUser_email(),register.getUser_phone(),register.getUser_address());
	}
}

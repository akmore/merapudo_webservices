package com.meraPUDO.main.webservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.meraPUDO.main")
public class MeraPudOappApplication {

	public static void main(String[] args) {
		SpringApplication.run(MeraPudOappApplication.class, args);
	}

}
